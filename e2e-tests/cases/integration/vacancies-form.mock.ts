import assert = require('assert');
import { expectedRequestBodyPostContactsWithVacancy } from '../../constants/request/contacts-with-vacancy-request-body.mock';

const { ListOfVacancies, VacancyCard } = inject();
const contactsUrl = '/api/contacts';
const userName = 'ТЕСТ';
const userPhone = '0000000000';
const userEmail = 'Tierra18@hotmail.com';
const portfolio = 'portfolio';

Feature('Отправка отклика на вакансию');

Before(I => {
    I.startMocking('vacancies-test');
    I.amOnPage('https://m2.ru/vacancies/any/develop');
});

After(I => {
    I.stopMocking();
});

Scenario('Успешная отправка отклика на конкретную вакансию', async I => {
    I.mockRequest('POST', contactsUrl, 201, {});
    ListOfVacancies.selectVacancy();
    VacancyCard.openVacancyForm();
    VacancyCard.fillName(userName);
    VacancyCard.fillPhone(userPhone);
    VacancyCard.fillEmail(userEmail);
    VacancyCard.fillPortfolio(portfolio);
    const request = await I.clickAndWaitRequest(VacancyCard.buttons.submitFormButton, contactsUrl, 'POST');
    const requestBody = JSON.parse(request.postData());
    I.waitForText('Отклик успешно отправлен', 6);
    assert.deepStrictEqual(requestBody, expectedRequestBodyPostContactsWithVacancy);
});