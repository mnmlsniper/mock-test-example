const commonConfig = require('./codecept.conf').config;

exports.config = {
    ...commonConfig,
    helpers: {
        ...commonConfig.helpers,
        Puppeteer: {
            ...commonConfig.helpers.Puppeteer,
            url: process.env.TEST_URL,
        },
    },
    tests: './cases/integration/*.mock.ts',
};
