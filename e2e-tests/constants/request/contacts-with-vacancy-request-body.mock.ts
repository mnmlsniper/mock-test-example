export const expectedRequestBodyPostContactsWithVacancy = {
    contact: {
        email: 'Tierra18@hotmail.com',
        legalAgreement: true,
        name: 'ТЕСТ',
        phone: '+7 (000) 000-00-00',
        position: 'msk-develop-senior-java-developer',
        region: 'msk',
        summary: 'portfolio',
    },
    position: 'Senior Java developer, (Москва)',
    region: 'msk',
};
