class BrowserHelper extends Helper {
    async clickAndWaitRequest(locator: string, requestUrl: string, method?: 'POST' | 'GET') {
        const { page } = this.helpers.Puppeteer;
        const isDesiredRequest = request => {
            if (method) {
                if (request.method() !== method) return false;
            }

            return request.url().includes(requestUrl);
        };

        const [request] = await Promise.all([page.waitForRequest(isDesiredRequest), page.click(locator)]);
        return request;
    }
}

export type BrowserHelpers = InstanceType<typeof BrowserHelper>;

module.exports = BrowserHelper;
