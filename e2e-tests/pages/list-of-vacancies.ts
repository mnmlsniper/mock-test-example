I = inject().I;

module.exports = {
    fields: {
        name: '[data-test="name"]',
        phone: '[data-test="phone"]',
        email: '[data-test="email"]',
        portfolio: '[data-test="portfolio"]',
        position: '[data-test="custom-position"]',
    },
    buttons: {
        backButton: '[data-test="vacancy-back-button"]',
        vacancy: 'a[href="/vacancies/any/develop/msk-develop-senior-java-developer"]',
        submitFormButton: '[data-test="apply-button"]',
        openFormButton: '[data-test="vacancy-list-form-button"]',
    },

    backToAboutPage() {
        I.click({ css: this.buttons.backButton });
    },
    selectVacancy() {
        I.click({ css: this.buttons.vacancy });
    },
    openVacancyForm() {
        I.scrollTo({ css: this.buttons.openFormButton });
        I.click({ css: this.buttons.openFormButton });
        I.waitForText('Откликнуться');
    },
    fillName(value: string) {
        I.clickAndFill({ css: this.fields.name }, value);
    },
    fillPhone(value: string) {
        I.clickAndFill({ css: this.fields.phone }, value);
    },
    fillEmail(value: string) {
        I.clickAndFill({ css: this.fields.email }, value);
    },
    fillPosition(value: string) {
        I.clickAndFill({ css: this.fields.position }, value);
    },
    fillPortfolio(value: string) {
        I.clickAndFill({ css: this.fields.portfolio }, value);
    },
    submitVacancyForm() {
        I.click({ css: this.buttons.submitFormButton });
        I.waitForText('Отклик успешно отправлен', 6);
    },
};
