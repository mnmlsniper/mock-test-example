I = inject().I;

module.exports = {
    fields: {
        name: '[data-test="name"]',
        phone: '[data-test="phone"]',
        email: '[data-test="email"]',
        portfolio: '[data-test="portfolio"]',
    },
    buttons: {
        submitFormButton: '[data-test="apply-button"]',
        openFormButton: '[data-test="vacancy-form-button"]',
    },

    openVacancyForm() {
        I.click({ css: this.buttons.openFormButton });
    },
    fillName(value: string) {
        I.clickAndFill({ css: this.fields.name }, value);
    },
    fillPhone(value: string) {
        I.clickAndFill({ css: this.fields.phone }, value);
    },
    fillEmail(value: string) {
        I.clickAndFill({ css: this.fields.email }, value);
    },
    fillPortfolio(value: string) {
        I.clickAndFill({ css: this.fields.portfolio }, value);
    },
    submitVacancyForm() {
        I.click({ css: this.buttons.submitFormButton });
        I.waitForText('Отклик успешно отправлен', 6);
    },
};
