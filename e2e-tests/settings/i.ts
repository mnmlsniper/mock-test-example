// in this file you can append custom step methods to 'I' object

module.exports = function() {
    return actor({
        waitAndClick(text: string, time?: number) {
            this.waitForText(text, time);
            this.click(text);
        },
        clickAndFill(locator, text: string) {
            this.click(locator);
            this.fillField(locator, text);
        },
        seeElementInViewport(locator) {
            this.moveCursorTo(locator);
        },
    });
};

// eslint-disable-next-line prefer-const, @typescript-eslint/no-unused-vars
let I: CodeceptJS.I;
